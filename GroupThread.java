/* This thread does all the work. It communicates with the client through Envelopes.
 * 
 */
import java.lang.Thread;
import java.net.Socket;
import java.net.URLDecoder;
import java.io.*;
import java.util.*;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class GroupThread extends Thread 
{
	private final Socket socket;
	private GroupServer my_gs;
	private SecretKey SharedKey;
	
	public GroupThread(Socket _socket, GroupServer _gs)
	{
		socket = _socket;
		my_gs = _gs;
		SharedKey = null;
	}
	
	public void run()
	{
		boolean proceed = true;

		try
		{
			//Announces connection and opens object streams
			System.out.println("*** New connection from " + socket.getInetAddress() + ":" + socket.getPort() + "***");
			final ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
			final ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
			
			do
			{
				Envelope message = (Envelope)input.readObject();
				System.out.println("Request received: " + message.getMessage());
				Envelope response;
				
				if(message.getMessage().equals("PUBLICKEYREQUEST"))
				{
					if(message.getObjContents().size() < 1)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("OK");
						response.addObject(my_gs.publicKey);
					}
					output.writeObject(response);
				}
				else if(message.getMessage().equals("NONCE"))
				{
					if(message.getObjContents().size() < 1)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("OK");
						String username = (String)message.getObjContents().get(0);
						String encryptedNonce = (String)message.getObjContents().get(1);
						GenerateRSAKeys rsa = new GenerateRSAKeys();
						/////////////////////////TODO remove
						System.out.println("encryptedNonce:\""+encryptedNonce+"\"");
						////////////////////////////////////
						String decryptedNonce = rsa.decrypt(encryptedNonce, my_gs.privateKey);
						
						response.addObject(decryptedNonce);
					}
					output.writeObject(response);
				}
				else if(message.getMessage().equals("SHAREDKEY"))
				{
					if(message.getObjContents().size() < 2)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("OK");
						String username = (String)message.getObjContents().get(0);
						String encryptedInfo = (String)message.getObjContents().get(1); // this should be username+password+KS
						GenerateRSAKeys rsa = new GenerateRSAKeys();
						String decryptedInfo = rsa.decrypt(encryptedInfo, my_gs.privateKey);
						String[] info = decryptedInfo.split(" ");
						for(int i = 0; i < 2; i++) { // just decode the first two space-separated parts; the remaining part(s) is/are the key
							info[i] = URLDecoder.decode(info[i], "UTF-8");
						}
						
						/////////////////////TODO remove
						//for(String s : info) {
						//	System.out.println(s);
						//}
						
						// validate credentials
						username = info[0];
						String password = info[1];
						if(!my_gs.userList.checkPassword(username, password)) {
							System.out.println("Wrong username/password.");
							return;
						}
						
						String sharedKey = decryptedInfo.substring(decryptedInfo.indexOf(" ")).substring(decryptedInfo.indexOf(" ")); // drop the first two space-separated parts; the rest is the key
						byte[] sharedKeyBytes = sharedKey.getBytes("ISO-8859-1");
						ByteArrayInputStream bis = new ByteArrayInputStream(sharedKeyBytes);
						ObjectInputStream ois = new ObjectInputStream(bis);

						//oos.writeObject(sharedKey);
						//byte[] keyByte = bos.toByteArray();
						//sharedKey = keyByte.toString();
						//ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(keyByte));
						Object o = ois.readObject();
						SecretKey secretKey = (SecretKey) o;
						
						UserToken yourToken = createToken(username); //Create a token
						String userToken = ((Token)yourToken).stringify(my_gs.privateKey);
						if(userToken == null) {
							System.err.println("Token string is null.");
							output.close();
							return;
						}
						byte[] encryptedToken;
						Cipher cipher = Cipher.getInstance("AES", "BC");
						cipher.init(Cipher.ENCRYPT_MODE, secretKey);
						encryptedToken = cipher.doFinal(userToken.getBytes("ISO-8859-1"));
						
						//Respond to the client. On error, the client will receive a null token
						response.addObject(new String(encryptedToken, "ISO-8859-1"));
						output.writeObject(response);
					}
					output.writeObject(response);
				}
				else if(message.getMessage().equals("CUSER")) //Client wants to create a user
				{
					if(message.getObjContents().size() < 2)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								Cipher cipher = Cipher.getInstance("AES", "BC");
								cipher.init(Cipher.DECRYPT_MODE, SharedKey);
								
								String username = (String)message.getObjContents().get(0); //Extract the username
								String yourToken = (String)message.getObjContents().get(1); //Extract the token
								byte[] passwordEncrypted = (byte[])message.getObjContents().get(2); // Extact the encrypted password byte array
								
								String decryptedUsername  = new String(cipher.doFinal(username.getBytes("UTF-8")), "UTF-8");
								String decryptedToken  = new String(cipher.doFinal(yourToken.getBytes("UTF-8")), "UTF-8");
								String decryptedPassword = new String(cipher.doFinal(passwordEncrypted), "UTF-8");
								
								Token token = Token.fromString(decryptedToken, 360000, my_gs.publicKey);
								
								if(createUser(decryptedUsername, decryptedPassword,  token))
								{
									response = new Envelope("OK"); //Success
								}
							}
						}
					}
					
					output.writeObject(response);
				}
				else if(message.getMessage().equals("DUSER")) //Client wants to delete a user
				{
					
					if(message.getObjContents().size() < 2)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								Cipher cipher = Cipher.getInstance("AES", "BC");
								cipher.init(Cipher.DECRYPT_MODE, SharedKey);
								
								String username = (String)message.getObjContents().get(0); //Extract the username
								String yourToken = (String)message.getObjContents().get(1); //Extract the token
								
								String decryptedUsername  = new String(cipher.doFinal(username.getBytes("UTF-8")), "UTF-8");
								String decryptedToken  = new String(cipher.doFinal(yourToken.getBytes("UTF-8")), "UTF-8");
								
								Token token = Token.fromString(decryptedToken, 360000, my_gs.publicKey);
								
								if(deleteUser(decryptedUsername, token))
								{
									response = new Envelope("OK"); //Success
								}
							}
						}
					}
					
					output.writeObject(response);
				}
				else if(message.getMessage().equals("CGROUP")) //Client wants to create a group
				{
					if(message.getObjContents().size() < 2)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								Cipher cipher = Cipher.getInstance("AES", "BC");
								cipher.init(Cipher.DECRYPT_MODE, SharedKey);
								
								String groupname = (String)message.getObjContents().get(0); //Extract the username
								String yourToken = (String)message.getObjContents().get(1); //Extract the token
								
								String decryptedGroupname  = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
								String decryptedToken  = new String(cipher.doFinal(yourToken.getBytes("UTF-8")), "UTF-8");
								
								Token token = Token.fromString(decryptedToken, 360000, my_gs.publicKey);
								
								if(addGroup(decryptedGroupname, token))
								{
									response = new Envelope("OK"); //Success
								}
							}
						}
					}
					
					output.writeObject(response);
				}
				else if(message.getMessage().equals("DGROUP")) //Client wants to delete a group
				{
					if(message.getObjContents().size() < 2)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								Cipher cipher = Cipher.getInstance("AES", "BC");
								cipher.init(Cipher.DECRYPT_MODE, SharedKey);
								
								String groupname = (String)message.getObjContents().get(0); //Extract the username
								String yourToken = (String)message.getObjContents().get(1); //Extract the token
								
								String decryptedGroupname  = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
								String decryptedToken  = new String(cipher.doFinal(yourToken.getBytes("UTF-8")), "UTF-8");
								
								Token token = Token.fromString(decryptedToken, 360000, my_gs.publicKey);
								
								if(deleteGroup(decryptedGroupname, token))
								{
									response = new Envelope("OK"); //Success
								}
							}
						}
					}
					
					output.writeObject(response);
				}
				else if(message.getMessage().equals("LMEMBERS")) //Client wants a list of members in a group
				{
				    if(message.getObjContents().size() < 2)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								Cipher cipher = Cipher.getInstance("AES", "BC");
								cipher.init(Cipher.DECRYPT_MODE, SharedKey);
								
								String groupname = (String)message.getObjContents().get(0); //Extract the username
								String yourToken = (String)message.getObjContents().get(1); //Extract the token
								
								String decryptedGroupname  = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
								String decryptedToken  = new String(cipher.doFinal(yourToken.getBytes("UTF-8")), "UTF-8");
								
								Token token = Token.fromString(decryptedToken, 360000, my_gs.publicKey);
								
								List<String> members = listMembers(decryptedGroupname, token);
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								ObjectOutputStream oos = new ObjectOutputStream(baos);
								oos.writeObject(members);
								byte[] mem = baos.toByteArray();
								
								if(!members.isEmpty())
								{
									response = new Envelope("OK"); //Success
									
									response.addObject(mem);
								}
							}
						}
					}
					
					output.writeObject(response);
				}
				else if(message.getMessage().equals("AUSERTOGROUP")) //Client wants to add user to a group
				{
				    if(message.getObjContents().size() < 3)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								if(message.getObjContents().get(2) != null)
								{
									String username = (String)message.getObjContents().get(0); //Extract the username
									String groupname = (String)message.getObjContents().get(1); //Extract the groupname
									String yourToken = (String)message.getObjContents().get(2); //Extract the token

									Cipher cipher = Cipher.getInstance("AES", "BC");
									cipher.init(Cipher.DECRYPT_MODE, SharedKey);
									
									String decryptedUsername = new String(cipher.doFinal(username.getBytes("UTF-8")), "UTF-8");
									String decryptedGroupname  = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
									String decryptedToken  = new String(cipher.doFinal(yourToken.getBytes("UTF-8")), "UTF-8");
									
									Token token = Token.fromString(decryptedToken, 360000, my_gs.publicKey);
									
									if(addUserToGroup(decryptedGroupname, decryptedUsername, token))
									{
										response = new Envelope("OK"); //Success
									}
								}
							}
						}
					}
					
					output.writeObject(response);
				}
				else if(message.getMessage().equals("RUSERFROMGROUP")) //Client wants to remove user from a group
				{
				    if(message.getObjContents().size() < 3)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								if(message.getObjContents().get(2) != null)
								{
									String username = (String)message.getObjContents().get(0); //Extract the username
									String groupname = (String)message.getObjContents().get(1); //Extract the groupname
									String yourToken = (String)message.getObjContents().get(2); //Extract the token

									Cipher cipher = Cipher.getInstance("AES", "BC");
									cipher.init(Cipher.DECRYPT_MODE, SharedKey);
									
									String decryptedUsername = new String(cipher.doFinal(username.getBytes("UTF-8")), "UTF-8");
									String decryptedGroupname  = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
									String decryptedToken  = new String(cipher.doFinal(yourToken.getBytes("UTF-8")), "UTF-8");
									
									Token token = Token.fromString(decryptedToken, 360000, my_gs.publicKey);

									if(removeUserFromGroup(decryptedGroupname, decryptedUsername, token))
									{
										response = new Envelope("OK"); //Success
									}
								}
							}
						}
					}
					output.writeObject(response);
				}
				else if(message.getMessage().equals("DISCONNECT")) //Client wants to disconnect
				{
					socket.close(); //Close the socket
					proceed = false; //End this communication loop
				}
				else
				{
					response = new Envelope("FAIL"); //Server does not understand client request
					output.writeObject(response);
				}
			}while(proceed);	
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
	
	//Method to create tokens
	private UserToken createToken(String username) 
	{
		//Check that user exists
		if(my_gs.userList.checkUser(username))
		{
			//Issue a new token with server's name, user's name, and user's groups
			UserToken yourToken = new Token(my_gs.name, username, my_gs.userList.getUserGroups(username));
			return yourToken;
		}
		else
		{
			return null;
		}
	}
	
	
	//Method to create a user
	private boolean createUser(String username, String password, UserToken yourToken)
	{
		String requester = yourToken.getSubject();
		
		//Check if requester exists
		if(my_gs.userList.checkUser(requester))
		{
			//Get the user's groups
			ArrayList<String> temp = my_gs.userList.getUserGroups(requester);
			//requester needs to be an administrator
			if(temp.contains("ADMIN"))
			{
				//Does user already exist?
				if(my_gs.userList.checkUser(username))
				{
					return false; //User already exists
				}
				else
				{
					my_gs.userList.addUser(username, password);
					return true;
				}
			}
			else
			{
				return false; //requester not an administrator
			}
		}
		else
		{
			return false; //requester does not exist
		}
	}
	
	//Method to delete a user
	private boolean deleteUser(String username, UserToken yourToken)
	{
		String requester = yourToken.getSubject();
		
		//Does requester exist?
		if(my_gs.userList.checkUser(requester))
		{
			ArrayList<String> temp = my_gs.userList.getUserGroups(requester);
			//requester needs to be an administer
			if(temp.contains("ADMIN"))
			{
				//Does user exist?
				if(my_gs.userList.checkUser(username))
				{
					//User needs deleted from the groups they belong
					ArrayList<String> deleteFromGroups = new ArrayList<String>();
					
					//This will produce a hard copy of the list of groups this user belongs
					for(int index = 0; index < my_gs.userList.getUserGroups(username).size(); index++)
					{
						deleteFromGroups.add(my_gs.userList.getUserGroups(username).get(index));
					}
					
					//Delete the user from the groups
					//If user is the owner, removeMember will automatically delete group!
					for(int index = 0; index < deleteFromGroups.size(); index++)
					{
						my_gs.groupList.removeMember(username, deleteFromGroups.get(index));
					}
					
					//If groups are owned, they must be deleted
					ArrayList<String> deleteOwnedGroup = new ArrayList<String>();
					
					//Make a hard copy of the user's ownership list
					for(int index = 0; index < my_gs.userList.getUserOwnership(username).size(); index++)
					{
						deleteOwnedGroup.add(my_gs.userList.getUserOwnership(username).get(index));
					}
					
					//Delete owned groups
					for(int index = 0; index < deleteOwnedGroup.size(); index++)
					{
						//Use the delete group method. Token must be created for this action
						deleteGroup(deleteOwnedGroup.get(index), new Token(my_gs.name, username, deleteOwnedGroup));
					}
					
					//Delete the user from the user list
					my_gs.userList.deleteUser(username);
					
					return true;	
				}
				else
				{
					return false; //User does not exist
					
				}
			}
			else
			{
				return false; //requester is not an administer
			}
		}
		else
		{
			return false; //requester does not exist
		}
	}

	private synchronized boolean deleteGroup(String groupName, UserToken owner) {
		GroupList gl = my_gs.groupList;
		UserList ul = my_gs.userList;

		/* check ownership */
		if(!gl.isOwner(owner, groupName)) {
			return false;
		}

		/* for each user in that group, delete the group from that user's group of lists */
		List<String> members = gl.listMembers(groupName, owner); // ask the group list for the users in the group
		for(String member : members) {
			ul.removeGroup(member, groupName);
		}

		/* delete the group from the group list */
		gl.removeGroup(groupName, owner);

		return true;
	}
	
	private synchronized boolean addGroup(String groupName, UserToken ownerToken) {
		GroupList gl = my_gs.groupList;
		UserList ul = my_gs.userList;
		
		/* add the new group to the group list */
		gl.addGroup(groupName, ownerToken);
		
		/* add the new group to the user in the user list */
		ul.addGroup(ownerToken.getSubject(), groupName);
		
		return true;
	}
	
	private synchronized List<String> listMembers(String groupName, UserToken ownerToken) {
		GroupList gl = my_gs.groupList;
		
		/* check ownership */
		if(!gl.isOwner(ownerToken, groupName)) {
			return null;
		}
	
		return gl.listMembers(groupName, ownerToken);
	}
	
	private synchronized boolean addUserToGroup(String groupName, String username, UserToken ownerToken) {
		GroupList gl = my_gs.groupList;
		UserList ul = my_gs.userList;
		
		/* check ownership */
		if(!gl.isOwner(ownerToken, groupName)) {
			return false;
		}
		
		/* add the user to the group in the group list */
		gl.addUserToGroup(groupName, ownerToken, username);
		
		/* add the group to the user in the user list */
		ul.addGroup(username, groupName);
		
		return true;
	}
	
	private synchronized boolean removeUserFromGroup(String groupName, String username, UserToken ownerToken) {
		GroupList gl = my_gs.groupList;
		UserList ul = my_gs.userList;
		
		/* check ownership */
		if(!gl.isOwner(ownerToken, groupName)) {
			return false;
		}
		
		/* remove the user from the group in the group list */
		gl.removeMember(username, groupName);
		
		/* remove the group from the user in the user list */
		ul.removeGroup(username, groupName);
		
		return true;
	}
}
