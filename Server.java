import java.net.Socket;
import java.security.PrivateKey;
import java.security.PublicKey;

public abstract class Server {
	
	protected int port;
	public String name;
	abstract void start();
	
	protected PublicKey publicKey;
	protected PrivateKey privateKey;
	protected PublicKey groupserverpublickey;
	
	public Server(int _SERVER_PORT, String _serverName) {
		port = _SERVER_PORT;
		name = _serverName; 
	}
	
		
	public int getPort() {
		return port;
	}
	
	public String getName() {
		return name;
	}

}
