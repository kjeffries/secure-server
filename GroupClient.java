/* Implements the GroupClient Interface */

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.PublicKey;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

public class GroupClient extends Client implements GroupClientInterface {
 
	public Key SharedKey;
	 public UserToken getToken(String username, String password)
	 {
		try
		{
			UserToken token = null;
			Envelope message = null, response = null;
			PublicKey GroupServerPubKey = null;
		 	//REQUEST SERVER'S PUBLIC KEY
			message = new Envelope("PUBLICKEYREQUEST");
			message.addObject(username);
			output.writeObject(message);
			
			response = (Envelope)input.readObject();
			
			//if we get "OK", we should have public key in response
			//inside this IF, we must do the entire interaction
			if(response.getMessage().equals("OK")) 
			{
				ArrayList<Object> temp = null;
				temp = response.getObjContents();
				if(temp.size() == 1)
				{
					GroupServerPubKey = (PublicKey)temp.get(0);
					
					SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
					//generate a random number (this will be the nonce)
					//it will be a string so we can encrypt/decrypt
					
					GenerateRSAKeys rsa = new GenerateRSAKeys();
					
					String randomNum = String.valueOf(prng.nextInt());
					String encryptedNonce = rsa.encrypt(randomNum, GroupServerPubKey);
					
					/////////////////////////////////TODO remove
					//System.out.println("Nonce:\""+randomNum+"\"");
					//System.out.println("Encrypted Nonce:\""+encryptedNonce+"\""); 
					//System.out.println(", which has length "+encryptedNonce.length());
					///////////////////////////////////////////
					
					message = new Envelope("NONCE");
					message.addObject(username);
					message.addObject(encryptedNonce);
					output.writeObject(message);
					
					response = (Envelope)input.readObject();
					if(response.getMessage().equals("OK"))
					{
						temp = response.getObjContents();
						
						//if we get here, they sent us back a decrypted nonce
						if(temp.size() == 1)
						{
							String decryptedNonce = (String)temp.get(0);
							
							//if they sent us back the nonce correctly, server is verified
							if(decryptedNonce.equals(randomNum))
							{
								KeyGenerator keygen;
								//here we need to generate a shared 128-bit AES key
								keygen = KeyGenerator.getInstance("AES", "BC");
								keygen.init(new SecureRandom());
								
								//generate our 128 bit keystring
								SharedKey = keygen.generateKey();
								

								String first = URLEncoder.encode(username, "UTF-8") + " " + URLEncoder.encode(password, "UTF-8") + " ";
								byte[] userPass = first.getBytes("UTF-8");
								
								ByteArrayOutputStream bos = new ByteArrayOutputStream();
								ObjectOutputStream oos = new ObjectOutputStream(bos);

								oos.writeObject(SharedKey);
								byte[] keyByte = bos.toByteArray();
								
								//List<Byte> list = (Arrays.asList(userPass));
								//list.addAll(Arrays.asList(keyByte));
								List<Byte> list = new ArrayList<Byte>();
								for(byte b : userPass) {
									list.add(b);
								}
								for(byte b : keyByte) {
									list.add(b);
								}
								//byte[] input = list.toArray(new byte[list.size()]);
								byte[] inputArray = new byte[list.size()];
								for(int i = 0; i < inputArray.length; i++) {
									inputArray[i] = list.get(i);
								}
										
								String encrypted = rsa.encrypt(new String(inputArray, "ISO-8859-1"), GroupServerPubKey);
								
								message = new Envelope("SHAREDKEY");
								message.addObject(username);
								message.addObject(encrypted);
								output.writeObject(message);
								
								response = (Envelope)input.readObject();
								
								temp = response.getObjContents();
						
								//if we get here, they sent us back an encrypted token
								if(temp.size() == 1)
								{
									Cipher cipher = Cipher.getInstance("AES", "BC");;

									cipher.init(Cipher.DECRYPT_MODE, SharedKey);
									String encryptedToken = (String)temp.get(0);
									byte[] decryptedToken = cipher.doFinal(encryptedToken.getBytes("ISO-8859-1"));
									
									String tok = decryptedToken.toString();
									token = Token.fromString(tok, 0, null);
									return token;
								}
							}
						}
					}
				}
			}
			return null;
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
			return null;
		}
		
	 }
	 
	 public boolean createUser(String username, String password, UserToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to create a user
				Cipher cipher = Cipher.getInstance("AES", "BC");
				cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
				message = new Envelope("CUSER");
				String encryptedName = new String(cipher.doFinal(username.getBytes("UTF-8")), "UTF-8");
				message.addObject(encryptedName); //Add user name string
				String tokenString = ((Token)token).stringify(null);
				String sendToken = new String(cipher.doFinal(tokenString.getBytes("UTF-8")), "ISO-8859-1");
				message.addObject(sendToken); //Add the requester's token
				message.addObject(cipher.doFinal(password.getBytes("UTF-8"))); // add the password, encrypted, as a byte array
				output.writeObject(message);
			
				response = (Envelope)input.readObject();
				
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 
	 public boolean deleteUser(String username, UserToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
			 
				//Tell the server to delete a user
				message = new Envelope("DUSER");
				
				Cipher cipher = Cipher.getInstance("AES", "BC");
				cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
				String encryptedName = new String(cipher.doFinal(username.getBytes("UTF-8")), "UTF-8");
				message.addObject(encryptedName); //Add user name string
				
				String tokenString = ((Token)token).stringify(null);
				String sendToken = new String(cipher.doFinal(tokenString.getBytes("UTF-8")), "ISO-8859-1");
				message.addObject(sendToken);
				output.writeObject(message);
			
				response = (Envelope)input.readObject();
				
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 
	 public boolean createGroup(String groupname, UserToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to create a group
				message = new Envelope("CGROUP");
				Cipher cipher = Cipher.getInstance("AES", "BC");
				cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
				String encryptedName = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
				message.addObject(encryptedName); //Add user name string
				
				String tokenString = ((Token)token).stringify(null);
				String sendToken = new String(cipher.doFinal(tokenString.getBytes("UTF-8")), "ISO-8859-1");
				message.addObject(sendToken);
				output.writeObject(message); 
			
				response = (Envelope)input.readObject();
				
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 
	 public boolean deleteGroup(String groupname, UserToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to delete a group
				message = new Envelope("DGROUP");
				Cipher cipher = Cipher.getInstance("AES", "BC");
				cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
				String encryptedName = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
				message.addObject(encryptedName); //Add user name string
				
				String tokenString = ((Token)token).stringify(null);
				String sendToken = new String(cipher.doFinal(tokenString.getBytes("UTF-8")), "ISO-8859-1");
				message.addObject(sendToken);
				output.writeObject(message); 
			
				response = (Envelope)input.readObject();
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 
	 @SuppressWarnings("unchecked")
	public List<String> listMembers(String group, UserToken token)
	 {
		 try
		 {
			Envelope message = null, response = null;
			//Tell the server to return the member list
			message = new Envelope("LMEMBERS");
			Cipher cipher = Cipher.getInstance("AES", "BC");
			cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
			String encryptedName = new String(cipher.doFinal(group.getBytes("UTF-8")), "UTF-8");
			message.addObject(encryptedName); //Add user name string
			
			String tokenString = ((Token)token).stringify(null);
			String sendToken = new String(cipher.doFinal(tokenString.getBytes("UTF-8")), "ISO-8859-1");
			message.addObject(sendToken);
			 output.writeObject(message); 
			 
			 response = (Envelope)input.readObject();
			 
			 //If server indicates success, return the member list
			 if(response.getMessage().equals("OK"))
			 {
				byte[] mem = (byte[])response.getObjContents().get(0);
				cipher.init(Cipher.DECRYPT_MODE, SharedKey);
				byte[] decrypted = cipher.doFinal(mem);
				ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(decrypted));
				
				return (List<String>)ois.readObject(); //This cast creates compiler warnings. Sorry.
			 }
				
			 return null;
			 
		 }
		 catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return null;
			}
	 }
	 
	 public boolean addUserToGroup(String username, String groupname, UserToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to add a user to the group
				message = new Envelope("AUSERTOGROUP");
				Cipher cipher = Cipher.getInstance("AES", "BC");
				cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
				String encryptedName = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
				message.addObject(encryptedName); //Add user name string
				
				String groupName = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
				message.addObject(groupName); //Add user name string
				
				String tokenString = ((Token)token).stringify(null);
				String sendToken = new String(cipher.doFinal(tokenString.getBytes("UTF-8")), "ISO-8859-1");
				message.addObject(sendToken);
				output.writeObject(message); 
			
				response = (Envelope)input.readObject();
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 
	 public boolean deleteUserFromGroup(String username, String groupname, UserToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to remove a user from the group
				message = new Envelope("RUSERFROMGROUP");
				Cipher cipher = Cipher.getInstance("AES", "BC");
				cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
				String encryptedName = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
				message.addObject(encryptedName); //Add user name string
				
				String groupName = new String(cipher.doFinal(groupname.getBytes("UTF-8")), "UTF-8");
				message.addObject(groupName); //Add user name string
				
				String tokenString = ((Token)token).stringify(null);
				String sendToken = new String(cipher.doFinal(tokenString.getBytes("UTF-8")), "ISO-8859-1");
				message.addObject(sendToken);
				output.writeObject(message);
			
				response = (Envelope)input.readObject();
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 
}
