import java.security.KeyPair;

/* Driver program for FileSharing File Server */

public class RunFileServer {
	
	public static void main(String[] args) {
		if (args.length > 0) {
			try {
				FileServer server = new FileServer(Integer.parseInt(args[0]));
				GenerateRSAKeys rsa = new GenerateRSAKeys();
				try
				{
					KeyPair kp = rsa.CreateRSAKeys(4096);
					server.publicKey = kp.getPublic();
					server.groupserverpublickey = GroupServerKeys.getPublicKey();
					server.privateKey = kp.getPrivate();
				}catch(Exception e){}
				
				server.start();
			}
			catch (NumberFormatException e) {
				System.out.printf("Enter a valid port number or pass no arguments to use the default port (%d)\n", FileServer.SERVER_PORT);
			}
		}
		else {
			FileServer server = new FileServer();
			server.start();
		}
	}

}
