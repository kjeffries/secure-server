/* File worker thread handles the business of uploading, downloading, and removing files for clients with valid tokens */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;

public class FileThread extends Thread
{
	private final Socket socket;
	private Token token;
	private Key SharedKey;
	private FileServer my_fs;
	
	private final long timestampTolerance = 360000; // 1 hour = 360000 milliseconds

	public FileThread(Socket _socket, FileServer _fs)
	{
		socket = _socket;
		my_fs = _fs;
	}

	public void run()
	{
		boolean proceed = true;
		try
		{
			System.out.println("*** New connection from " + socket.getInetAddress() + ":" + socket.getPort() + "***");
			final ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
			final ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
			Envelope response;

			do
			{
				Envelope e = (Envelope)input.readObject();
				System.out.println("Request received: " + e.getMessage());

				// Handler to list files that this user is allowed to see
				if(e.getMessage().equals("LFILES"))
				{
				    /* TODO: Write this handler */
					
					if(e.getObjContents().size() < 1)
					{
						response = new Envelope("FAIL-BADCONTENTS");
					}
					else
					{
						Cipher cipher = Cipher.getInstance("AES/ECB", "BC");
						cipher.init(Cipher.DECRYPT_MODE, SharedKey);
						byte[] tok = (byte[])e.getObjContents().get(0);
						String decrypted = new String(cipher.doFinal(tok), "UTF-8");
						
						UserToken token = Token.fromString(decrypted, 360000, my_fs.publicKey);
						if(token == null) {
							response = new Envelope("FAIL-BADTOKEN");
						}
						else {
							List<String> lfiles = new ArrayList<String>();
							
							List<String> usersGroups = token.getGroups();
							List<ShareFile> files = FileServer.fileList.getFiles();
							for(ShareFile file : files) { // for each file, check if it's in any of the allowable groups
								String group = file.getGroup();
								boolean inUsersGroups = false;
								for(String usersGroup : usersGroups) {
									if(usersGroup.equals(group)) {
										inUsersGroups = true;
										break;
									}
								}
								if(inUsersGroups) {
									lfiles.add(file.getPath());
								}
							}
							
							response = new Envelope("OK");
							
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							ObjectOutputStream oos = new ObjectOutputStream(baos);
							oos.writeObject(lfiles);
							byte[] out = baos.toByteArray();
							cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
							byte[] f = cipher.doFinal(out);
							
							response.addObject(f);
						}
					}
					
					output.writeObject(response);
				}
				if(e.getMessage().equals("UPLOADF"))
				{

					if(e.getObjContents().size() < 3)
					{
						response = new Envelope("FAIL-BADCONTENTS");
					}
					else
					{
						if(e.getObjContents().get(0) == null) {
							response = new Envelope("FAIL-BADPATH");
						}
						if(e.getObjContents().get(1) == null) {
							response = new Envelope("FAIL-BADGROUP");
						}
						if(e.getObjContents().get(2) == null) {
							response = new Envelope("FAIL-BADTOKEN");
						}
						else {
							Cipher cipher = Cipher.getInstance("AES/ECB", "BC");
							cipher.init(Cipher.DECRYPT_MODE, SharedKey);
							byte[] remotePath = (byte[])e.getObjContents().get(0);
							byte[] dpath = cipher.doFinal(remotePath);
							byte[] group = (byte[])e.getObjContents().get(1);
							byte[] dgroup = cipher.doFinal(group);
							byte[] yourToken = (byte[])e.getObjContents().get(2); //Extract token
							byte[] dtoken = cipher.doFinal(yourToken);
							
							String path = new String(dpath, "UTF-8");
							String g = new String(dgroup, "UTF-8");
							String tok = new String(dtoken, "UTF-8");
						
							UserToken token = Token.fromString(tok, 360000, my_fs.publicKey);

							if (FileServer.fileList.checkFile(path)) {
								System.out.printf("Error: file already exists at %s\n", path);
								response = new Envelope("FAIL-FILEEXISTS"); //Success
							}
							else if (!token.getGroups().contains(g)) {
								System.out.printf("Error: user missing valid token for group %s\n", g);
								response = new Envelope("FAIL-UNAUTHORIZED"); //Success
							}
							else  {
								File file = new File("shared_files/"+path.replace('/', '_'));
								file.createNewFile();
								FileOutputStream fos = new FileOutputStream(file);
								System.out.printf("Successfully created file %s\n", path.replace('/', '_'));

								response = new Envelope("READY"); //Success
								output.writeObject(response);

								e = (Envelope)input.readObject();
								while (e.getMessage().compareTo("CHUNK")==0) {
									fos.write(cipher.doFinal((byte[])e.getObjContents().get(0)), 0, (Integer)e.getObjContents().get(1));
									response = new Envelope("READY"); //Success
									output.writeObject(response);
									e = (Envelope)input.readObject();
								}

								if(e.getMessage().compareTo("EOF")==0) {
									System.out.printf("Transfer successful file %s\n", path);
									FileServer.fileList.addFile(token.getSubject(), g, path);
									response = new Envelope("OK"); //Success
								}
								else {
									System.out.printf("Error reading file %s from client\n", path);
									response = new Envelope("ERROR-TRANSFER"); //Success
								}
								fos.close();
							}
						}
					}

					output.writeObject(response);
				}
				else if (e.getMessage().compareTo("DOWNLOADF")==0) {

					Cipher cipher = Cipher.getInstance("AES/ECB", "BC");
					cipher.init(Cipher.DECRYPT_MODE, SharedKey);
					byte[] path = (byte[])e.getObjContents().get(0);
					byte[] token = (byte[])e.getObjContents().get(1); //Extract token
					byte[] dpath = cipher.doFinal(path);
					byte[] dtoken = cipher.doFinal(token);

					
					String remotePath = new String(dpath, "UTF-8");
					String tok = new String(dtoken, "UTF-8");
						
					UserToken tokens = Token.fromString(tok, 360000, my_fs.groupserverpublickey);
			
					ShareFile sf = FileServer.fileList.getFile("/"+remotePath);
					if (sf == null) {
						System.out.printf("Error: File %s doesn't exist\n", remotePath);
						e = new Envelope("ERROR_FILEMISSING");
						output.writeObject(e);

					}
					else if (!tokens.getGroups().contains(sf.getGroup())){
						System.out.printf("Error user %s doesn't have permission\n", tokens.getSubject());
						e = new Envelope("ERROR_PERMISSION");
						output.writeObject(e);
					}
					else {

						try
						{
							File f = new File("shared_files/_"+remotePath.replace('/', '_'));
						if (!f.exists()) {
							System.out.printf("Error file %s missing from disk\n", "_"+remotePath.replace('/', '_'));
							e = new Envelope("ERROR_NOTONDISK");
							output.writeObject(e);

						}
						else {
							FileInputStream fis = new FileInputStream(f);

							do {
								cipher = Cipher.getInstance("AES/ECB", "BC");
								cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
								byte[] buf = new byte[4096];
								if (e.getMessage().compareTo("DOWNLOADF")!=0) {
									System.out.printf("Server error: %s\n", e.getMessage());
									break;
								}
								e = new Envelope("CHUNK");
								int n = fis.read(buf); //can throw an IOException
								if (n > 0) {
									System.out.printf(".");
								} else if (n < 0) {
									System.out.println("Read error");

								}

								byte[] enc = cipher.doFinal(buf);
								//e.addObject(buf); // commented out by Kevin -- this should be encrypted
								e.addObject(enc);
								e.addObject(new Integer(n));

								output.writeObject(e);

								e = (Envelope)input.readObject();


							}
							while (fis.available()>0);

							//If server indicates success, return the member list
							if(e.getMessage().compareTo("DOWNLOADF")==0)
							{

								e = new Envelope("EOF");
								output.writeObject(e);

								e = (Envelope)input.readObject();
								if(e.getMessage().compareTo("OK")==0) {
									System.out.printf("File data upload successful\n");
								}
								else {

									System.out.printf("Upload failed: %s\n", e.getMessage());

								}

							}
							else {

								System.out.printf("Upload failed: %s\n", e.getMessage());

							}
						}
						}
						catch(Exception e1)
						{
							System.err.println("Error: " + e.getMessage());
							e1.printStackTrace(System.err);

						}
					}
				}
				else if (e.getMessage().compareTo("DELETEF")==0) {

					Cipher cipher = Cipher.getInstance("AES/ECB", "BC");
					cipher.init(Cipher.ENCRYPT_MODE, SharedKey);
					byte[] path = (byte[])e.getObjContents().get(0);
					byte[] token = (byte[])e.getObjContents().get(1); //Extract token
					byte[] dpath = cipher.doFinal(path);
					byte[] dtoken = cipher.doFinal(token);
					
					String remotePath = new String(dpath, "UTF-8");
					String tok = new String(dtoken, "UTF-8");
				
					UserToken yourToken = Token.fromString(tok, 360000, my_fs.groupserverpublickey);
					ShareFile sf = FileServer.fileList.getFile("/"+remotePath);
					if (sf == null) {
						System.out.printf("Error: File %s doesn't exist\n", remotePath);
						e = new Envelope("ERROR_DOESNTEXIST");
					}
					else if (!yourToken.getGroups().contains(sf.getGroup())){
						System.out.printf("Error user %s doesn't have permission\n", yourToken.getSubject());
						e = new Envelope("ERROR_PERMISSION");
					}
					else {

						try
						{


							File f = new File("shared_files/"+"_"+remotePath.replace('/', '_'));

							if (!f.exists()) {
								System.out.printf("Error file %s missing from disk\n", "_"+remotePath.replace('/', '_'));
								e = new Envelope("ERROR_FILEMISSING");
							}
							else if (f.delete()) {
								System.out.printf("File %s deleted from disk\n", "_"+remotePath.replace('/', '_'));
								FileServer.fileList.removeFile("/"+remotePath);
								e = new Envelope("OK");
							}
							else {
								System.out.printf("Error deleting file %s from disk\n", "_"+remotePath.replace('/', '_'));
								e = new Envelope("ERROR_DELETE");
							}


						}
						catch(Exception e1)
						{
							System.err.println("Error: " + e1.getMessage());
							e1.printStackTrace(System.err);
							e = new Envelope(e1.getMessage());
						}
					}
					output.writeObject(e);

				}
				else if(e.getMessage().equals("GETKEY"))
				{
					response = new Envelope("OK");
					response.addObject(my_fs.publicKey);
					output.writeObject(response);
				}
				else if(e.getMessage().equals("NONCE"))
				{
					String numstring = (String)(e.getObjContents().get(0));

					GenerateRSAKeys rsa= new GenerateRSAKeys();
					String decryptedNonce = rsa.decrypt(numstring, my_fs.privateKey);

					response = new Envelope("OK");
					response.addObject(decryptedNonce); 
					output.writeObject(response);
				}
				else if(e.getMessage().equals("SKANDTOKEN"))
				{
					byte[] encryptedkey = (byte[])e.getObjContents().get(0);
					GenerateRSAKeys rsa = new GenerateRSAKeys();
					String decryptedkey = rsa.decrypt(new String(encryptedkey, "ISO-8859-1"), my_fs.privateKey);
					ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(decryptedkey.getBytes("ISO-8859-1")));
					SharedKey = (Key)ois.readObject();

					byte[] tokentmp = (byte[])e.getObjContents().get(1);
					Cipher cipherAES = Cipher.getInstance("AES", "BC");
					cipherAES.init(Cipher.DECRYPT_MODE, SharedKey);
					byte[] tokendecrypted = cipherAES.doFinal(tokentmp);
					String tempstring = new String (tokendecrypted, "UTF-8");
					token = Token.fromString(tempstring,3600000, my_fs.groupserverpublickey);
					if(token == null)
					{
						socket.close();
						proceed = false;
					}

				}
				else if(e.getMessage().equals("DISCONNECT"))
				{
					socket.close();
					proceed = false;
				}
			} while(proceed);
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}

}
