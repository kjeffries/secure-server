/* This list represents the users on the server */
import java.security.MessageDigest;
import java.security.Security;
import java.util.*;

import javax.crypto.Cipher;

import org.bouncycastle.jce.provider.BouncyCastleProvider;


	public class UserList implements java.io.Serializable {
	
		/**
		 * 
		 */
		private static final long serialVersionUID = 7600343803563417992L;
		private Hashtable<String, User> list = new Hashtable<String, User>();
		
		public synchronized void addUser(String username, String password)
		{
			User newUser = new User(password);
			list.put(username, newUser);
		}
		
		// true if this username-password combo is acceptable. False otherwise.
		public synchronized boolean checkPassword(String username, String password) {
			User user = list.get(username);
			if(user == null)
				return false;
			return user.checkPassword(password);
		}
		
		public synchronized void deleteUser(String username)
		{
			list.remove(username);
		}
		
		public synchronized boolean checkUser(String username)
		{
			if(list.containsKey(username))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public synchronized ArrayList<String> getUserGroups(String username)
		{
			return list.get(username).getGroups();
		}
		
		public synchronized ArrayList<String> getUserOwnership(String username)
		{
			return list.get(username).getOwnership();
		}
		
		public synchronized void addGroup(String user, String groupname)
		{
			list.get(user).addGroup(groupname);
		}
		
		public synchronized void removeGroup(String user, String groupname)
		{
			list.get(user).removeGroup(groupname);
		}
		
		public synchronized void addOwnership(String user, String groupname)
		{
			list.get(user).addOwnership(groupname);
		}
		
		public synchronized void removeOwnership(String user, String groupname)
		{
			list.get(user).removeOwnership(groupname);
		}
		
	
	class User implements java.io.Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 14575L;
		private ArrayList<String> groups;
		private ArrayList<String> ownership;
		
		private byte[] saltedHash; // salted hash of password
		
		public User(String password)
		{
			groups = new ArrayList<String>();
			ownership = new ArrayList<String>();
			
			saltedHash = calcSaltedHash(password);
		}
		
		public ArrayList<String> getGroups()
		{
			return groups;
		}
		
		public ArrayList<String> getOwnership()
		{
			return ownership;
		}
		
		public void addGroup(String group)
		{
			groups.add(group);
		}
		
		public void removeGroup(String group)
		{
			if(!groups.isEmpty())
			{
				if(groups.contains(group))
				{
					groups.remove(groups.indexOf(group));
				}
			}
		}
		
		public void addOwnership(String group)
		{
			ownership.add(group);
		}
		
		public void removeOwnership(String group)
		{
			if(!ownership.isEmpty())
			{
				if(ownership.contains(group))
				{
					ownership.remove(ownership.indexOf(group));
				}
			}
		}
		
		/* Returns true is provided password is valid, false otherwise */
		public boolean checkPassword(String password) {
			byte[] hashedInput = calcSaltedHash(password);
			return (Arrays.equals(hashedInput, saltedHash));
		}
		
		private byte[] calcSaltedHash(String s) {
			if(Security.getProvider("BC") == null) {
				Security.addProvider(new BouncyCastleProvider());
			}
			try {
				byte[] sBytes = s.getBytes("UTF-8");
				byte[] salt = { (byte)0x49, (byte)0x62 }; // randomly generated 2-byte salt
				byte[] stuffToHash = new byte[sBytes.length+salt.length];
				for(int i = 0; i < stuffToHash.length; i++) {
					if(i < sBytes.length) {
						stuffToHash[i] = sBytes[i];
					}
					else {
						stuffToHash[i] = salt[i-sBytes.length]; // append the salt
					}
				}
				byte[] hashedStuff = MessageDigest.getInstance("SHA-1", "BC").digest(stuffToHash);
				return hashedStuff;
			} catch (Exception e) { return null; }
		}
		
	}
	
}	
