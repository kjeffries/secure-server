/* FileClient provides all the client functionality regarding the file server */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.security.Key;
import java.security.KeyPair;
import java.security.SecureRandom;
import java.security.*;
import javax.crypto.*;
import java.io.FileWriter;
import java.util.*;
import java.io.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class FileClient extends Client implements FileClientInterface {

	public boolean delete(String filename, UserToken token) {
		SecretKey sessionkey;
		try {
			sessionkey = createskey(token);
		} catch(Exception e) { return false; }

		String remotePath;
		if (filename.charAt(0)=='/') {
			remotePath = filename.substring(1);
		}
		else {
			remotePath = filename;
		}

		Envelope env = new Envelope("DELETEF"); //Success
		try {
			Cipher cipherAES = Cipher.getInstance("AES");
			cipherAES.init(Cipher.ENCRYPT_MODE, sessionkey);

			env.addObject(cipherAES.doFinal(remotePath.getBytes("UTF-8")));
			env.addObject(cipherAES.doFinal(((Token)token).stringify(null).getBytes("UTF-8")));
		} catch(Exception e) {
			return false;
		}

		try {
			output.writeObject(env);
			env = (Envelope)input.readObject();

			if (env.getMessage().compareTo("OK")==0) {
				System.out.printf("File %s deleted successfully\n", filename);				
			}
			else {
				System.out.printf("Error deleting file %s (%s)\n", filename, env.getMessage());
				return false;
			}			
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}

		return true;
	}

	public boolean download(String sourceFile, String destFile, UserToken token) throws Exception {
		SecretKey sessionkey;
		try {
			sessionkey = createskey(token);
		} catch(Exception e) { return false; }

		if (sourceFile.charAt(0)=='/') {
			sourceFile = sourceFile.substring(1);
		}

		File file = new File(destFile);
		try {


			if (!file.exists()) {
				file.createNewFile();
				FileOutputStream fos = new FileOutputStream(file);

				Cipher cipherAES = Cipher.getInstance("AES");
				cipherAES.init(Cipher.ENCRYPT_MODE, sessionkey);

				Cipher cipherDECRYPTAES = Cipher.getInstance("AES");
	 			cipherDECRYPTAES.init(Cipher.DECRYPT_MODE, sessionkey);

				Envelope env = new Envelope("DOWNLOADF"); //Success
				env.addObject(cipherAES.doFinal(sourceFile.getBytes("UTF-8")));
				env.addObject(cipherAES.doFinal(((Token)token).stringify(null).getBytes("UTF-8")));
				output.writeObject(env); 

				env = (Envelope)input.readObject();

				while (env.getMessage().compareTo("CHUNK")==0) { 
					fos.write(cipherDECRYPTAES.doFinal((byte[])env.getObjContents().get(0)), 0, ((Integer)env.getObjContents().get(1)));
					System.out.printf(".");
					env = new Envelope("DOWNLOADF"); //Success
					output.writeObject(env);
					env = (Envelope)input.readObject();									
				}										
				fos.close();

				if(env.getMessage().compareTo("EOF")==0) {
					fos.close();
					System.out.printf("\nTransfer successful file %s\n", sourceFile);
					env = new Envelope("OK"); //Success
					output.writeObject(env);
				}
				else {
					System.out.printf("Error reading file %s (%s)\n", sourceFile, env.getMessage());
					file.delete();
					return false;								
				}
			}    

			else {
				System.out.printf("Error couldn't create file %s\n", destFile);
				return false;
			}


		} catch (IOException e1) {

			System.out.printf("Error couldn't create file %s\n", destFile);
			return false;


		}
		catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<String> listFiles(UserToken token) throws Exception {
		SecretKey sessionkey = createskey(token);

		try
		{
			Envelope message = null, e = null;
			//Tell the server to return the member list
			
			Cipher cipherAES = Cipher.getInstance("AES");
			cipherAES.init(Cipher.ENCRYPT_MODE, sessionkey);	

			Cipher cipherDECRYPTAES = Cipher.getInstance("AES");
	 		cipherDECRYPTAES.init(Cipher.DECRYPT_MODE, sessionkey);		

			message = new Envelope("LFILES");
			message.addObject(cipherAES.doFinal(((Token)token).stringify(null).getBytes("UTF-8"))); //Add requester's token
			output.writeObject(message); 

			e = (Envelope)input.readObject();

			//If server indicates success, return the member list
			if(e.getMessage().equals("OK"))
			{ 
				byte[] decrypted = cipherDECRYPTAES.doFinal((byte[])(e.getObjContents().get(0))); //This cast creates compiler warnings. Sorry.
				ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(decrypted));
				return (List<String>) ois.readObject();
			}

			return null;

		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
			return null;
		}
	}

	public boolean upload(String sourceFile, String destFile, String group,
			UserToken token) {

		SecretKey sessionkey;
		try {
			sessionkey = createskey(token);
		} catch(Exception e) { return false; }

		if (destFile.charAt(0)!='/') {
			destFile = "/" + destFile;
		}

		try
		{
			Cipher cipherAES = Cipher.getInstance("AES");
			cipherAES.init(Cipher.ENCRYPT_MODE, sessionkey);

			Cipher cipherDECRYPTAES = Cipher.getInstance("AES");
	 		cipherDECRYPTAES.init(Cipher.DECRYPT_MODE, sessionkey);

			Envelope message = null, env = null;
			//Tell the server to return the member list
			message = new Envelope("UPLOADF");
			message.addObject(cipherAES.doFinal(destFile.getBytes("UTF-8")));
			message.addObject(cipherAES.doFinal(group.getBytes("UTF-8")));
			message.addObject(cipherAES.doFinal(((Token)token).stringify(null).getBytes("UTF-8"))); //Add requester's token
			output.writeObject(message);


			FileInputStream fis = new FileInputStream(sourceFile);

			env = (Envelope)input.readObject();

			//If server indicates success, return the member list
			if(env.getMessage().equals("READY"))
			{ 
				System.out.printf("Meta data upload successful\n");

			}
			else {

				System.out.printf("Upload failed: %s\n", env.getMessage());
				return false;
			}


			do {
				byte[] buf = new byte[4096];
				if (env.getMessage().compareTo("READY")!=0) {
					System.out.printf("Server error: %s\n", env.getMessage());
					return false;
				}
				message = new Envelope("CHUNK");
				int n = fis.read(buf); //can throw an IOException
				if (n > 0) {
					System.out.printf(".");
				} else if (n < 0) {
					System.out.println("Read error");
					return false;
				}

				message.addObject(cipherAES.doFinal(buf));
				message.addObject(new Integer(n));

				output.writeObject(message);


				env = (Envelope)input.readObject();


			}
			while (fis.available()>0);		 

			//If server indicates success, return the member list
			if(env.getMessage().compareTo("READY")==0)
			{ 

				message = new Envelope("EOF");
				output.writeObject(message);

				env = (Envelope)input.readObject();
				if(env.getMessage().compareTo("OK")==0) {
					System.out.printf("\nFile data upload successful\n");
				}
				else {

					System.out.printf("\nUpload failed: %s\n", env.getMessage());
					return false;
				}

			}
			else {

				System.out.printf("Upload failed: %s\n", env.getMessage());
				return false;
			}

		}catch(Exception e1)
		{
			System.err.println("Error: " + e1.getMessage());
			e1.printStackTrace(System.err);
			return false;
		}
		return true;
	}

	public SecretKey createskey(UserToken token) throws Exception
	{
		BouncyCastleProvider bc = new BouncyCastleProvider();
		Security.addProvider(bc);
		//final ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
		//final ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());

		PublicKey publickey;
		SecretKey sessionkey;
		String pubkey;
		Envelope query = new Envelope("GETKEY");
		Envelope e;
		output.writeObject(query);

		e = (Envelope)input.readObject();

		if(e.getMessage().equals("OK"))
		{ 
			publickey = (PublicKey) e.getObjContents().get(0); //This cast creates compiler warnings. Sorry.

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(publickey);
			byte[] keyByte = bos.toByteArray();
			pubkey = new String(keyByte,"UTF-8");


			SecureRandom randomnumber;
			int rn = -1;
			File f = new File("publickeys.txt");
			if(f.exists())
			{
				Scanner scanner = new Scanner(f);
				int found = 0;
				while (scanner.hasNextLine()) 
				{
					String line = scanner.nextLine();
					if(line.equals(pubkey))
					{
						found = 1;
						randomnumber = SecureRandom.getInstance("SHA1PRNG", "BC");
						rn = randomnumber.nextInt();
						Cipher cipherRSA = Cipher.getInstance("RSA", "BC");
						cipherRSA.init(Cipher.ENCRYPT_MODE, publickey);

						Envelope env = new Envelope("NONCE");
						env.addObject(cipherRSA.doFinal(String.valueOf(rn).getBytes("UTF-8")));
						output.writeObject(env);
						break;
					}  		    
				}
				
				if(found == 0)
				{
					Scanner scan = new Scanner(System.in);
					System.out.println("Do you accept this server as trusted?(y/n): ");
					String ans = scan.nextLine();

					if(ans.equals("y"))
					{
						try
						{
							String filename= "publickey.txt";
							FileWriter fw = new FileWriter(filename,true); 
							fw.write(pubkey);
							fw.close();
						}
						catch(IOException ioe)
						{
							System.err.println("IOException: " + ioe.getMessage());
						}


						randomnumber = SecureRandom.getInstance("SHA1PRNG", "BC");
						rn = randomnumber.nextInt();
						Cipher cipherRSA = Cipher.getInstance("RSA", "BC");
						cipherRSA.init(Cipher.ENCRYPT_MODE, publickey);

						Envelope env = new Envelope("NONCE");
						env.addObject(cipherRSA.doFinal(String.valueOf(rn).getBytes("UTF-8")));
						output.writeObject(env);
					}
					else
					{
						System.out.println("Not a trusted server. Exiting!");
						return null;
					}
				}				
			}
			else
			{
				Scanner scan = new Scanner(System.in);
				System.out.println("Do you want to accept this server as trusted?(y/n): ");
				String ans = scan.nextLine();

				if(ans.equals("y"))
				{
					try
					{

						String filename= "publickey.txt";
						FileWriter fw = new FileWriter(filename,true); 
						fw.write(pubkey);
						fw.close();
					}
					catch(IOException ioe)
					{
						System.err.println("IOException: " + ioe.getMessage());
					}

					randomnumber = SecureRandom.getInstance("SHA1PRNG", "BC");
					rn = randomnumber.nextInt();
					Cipher cipherRSA = Cipher.getInstance("RSA", "BC");
					cipherRSA.init(Cipher.ENCRYPT_MODE, publickey);

					Envelope env = new Envelope("NONCE");
					env.addObject(cipherRSA.doFinal(String.valueOf(rn).getBytes("UTF-8")));
					output.writeObject(env);
				}
				else
				{
					System.out.println("Not a trusted server. Exiting!");
					return null;
				}
			}
			
			e = (Envelope)input.readObject();
			
			int sentnumber;
			if(e.getMessage().equals("OK"))
			{ 
				sentnumber = Integer.valueOf((String) e.getObjContents().get(0));

				if(sentnumber == rn)
				{
					KeyGenerator keyGenAES = KeyGenerator.getInstance("AES", "BC");
					keyGenAES.init(256);
					sessionkey = keyGenAES.generateKey();

					Cipher cipherRSA = Cipher.getInstance("RSA", "BC");
					cipherRSA.init(Cipher.ENCRYPT_MODE, publickey);

					Cipher cipherAES = Cipher.getInstance("AES", "BC");
					cipherAES.init(Cipher.ENCRYPT_MODE, sessionkey);

					Envelope env = new Envelope("SKANDTOKEN");
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					oos = new ObjectOutputStream(baos);
					oos.writeObject(sessionkey);
					byte[] sessionkeybytes = baos.toByteArray();

					env.addObject(cipherRSA.doFinal(sessionkeybytes));
					env.addObject(cipherAES.doFinal(((Token)token).stringify(null).getBytes("UTF-8")));
					output.writeObject(env);
				}
				else
				{
					System.out.println("These numbers do not match. You can't be trusted!");
					return null;
				}
				return sessionkey;
			}
		}
		return null;
	}

}

