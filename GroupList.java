import java.util.*;

public class GroupList implements java.io.Serializable {
	public static final long serialVersionUID = -4743L;
	
	private Map<String, Group> groupMap; // map from group name to Group
	
	public GroupList() {
		groupMap = new HashMap<String, Group>();
	}
	
	public synchronized void addGroup(String groupName, UserToken owner) {
		if(groupName != null && owner != null && !groupMap.containsKey(groupName)) {
			Group group = new Group(groupName, owner.getSubject());
			groupMap.put(groupName, group);
		}
	}
	
	public synchronized boolean removeGroup(String groupName, UserToken owner) {
		if(groupName != null && owner != null) {
			Group g = groupMap.get(groupName);
			if(g != null && owner.getSubject().equals(g.getOwner())) { // if it's the actual owner of this group
				groupMap.remove(g);
				return true;
			}
		}
		return false;
	}
	
	public synchronized void addUserToGroup(String groupName, UserToken owner, String personToAdd) {
		if(groupName != null && owner != null && personToAdd != null && groupMap.containsKey(groupName)) {
			Group g = groupMap.get(groupName);
			if(g != null && owner.getSubject().equals(g.getOwner())) { // if it's the actual owner of this group
				g.addUser(personToAdd);
			}
		}
	}
	
	public synchronized void removeMember(String username, String groupName) {
		Group g = groupMap.get(groupName);
		if(g != null) {
			g.removeUser(username);
		}
	}
	
	/*
	public void deleteUserFromGroup(String groupName, UserToken owner, UserToken personToDelete) {
		if(groupName != null && owner != null && personToDelete != null && groupMap.containsKey(groupName)) {
			Group g = groupMap.get(groupName);
			if(g != null && owner.getSubject().equals(g.getOwner())) { // if it's the actual owner of this group
				g.removeUser(personToDelete);
			}
		}
	}
	*/
	
	public synchronized List<String> listMembers(String groupName, UserToken owner) {
		if(groupName != null && owner != null) {
			Group g = groupMap.get(groupName);
			if(g != null && owner.getSubject().equals(g.getOwner())) { // if it's the actual owner of this group
				return g.listMembers();
			}
		}
		return null;
	}
	
	public synchronized boolean isOwner(UserToken owner, String groupName) {
		if(owner != null && groupName != null) {
			Group g = groupMap.get(groupName);
			if(g != null) {
				return (owner.getSubject().equals(g.getOwner()));
			}
		}
		return false;
	}
	
	/*
	public List<String> getGroups() {
		List<String> retval = new ArrayList<String>();
		for(String group : groupList) {
			retval.add(group);
		}
		return retval;
	}
	*/
}

class Group implements java.io.Serializable {
	public static final long serialVersionUID = -15780L;

	private String name;
	private String owner;
	private Set<String> members;

	public Group(String n, String o) {
		name = n;
		owner = o;
		members = new HashSet<String>();
		members.add(o);
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void addUser(UserToken user) {
		members.add(user.getSubject());
	}
	
	public void addUser(String user) {
		members.add(user);
	}
	
	public void removeUser(UserToken user) {
		members.remove(user.getSubject());
	}
	
	public void removeUser(String user) {
		members.remove(user);
	}
	
	public List<String> listMembers() {
		List<String> retval = new ArrayList<String>();
		for(String member : members) {
			retval.add(member);
		}
		return retval;
	}
}
