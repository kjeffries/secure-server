// Kevin

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.crypto.Cipher;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Token implements UserToken/*, java.io.Serializable*/ { // don't serialize Token. Instead, call stringify()

	//public static final long serialVersionUID = 18163L;

	private String issuer;
	private String subject;
	private Set<String> groupSet; // keep a set to be able to notice duplicates
	private List<String> groupList;
	
	// these two values should be set ONLY at signing and stringifying (and should be invalidated when the object is modified)
	private Long timestamp = null;
	private byte[] signedHash = null;
	
	public Token(String issu, String subj) {
		issuer = issu;
		subject = subj;
		groupSet = new HashSet<String>();
		groupList = new ArrayList<String>();
	}
	
	public Token(String issu, String subj, Collection<String> groups) {
		this(issu, subj);
		if(groups != null) {
			for(String group : groups) {
				addGroup(group);
			}
		}
	}
	
	public void addGroup(String group) {
		// token is being modified. Invalidate the signature.
		timestamp = null;
		signedHash = null;
		
		if(group != null) {
			if(groupSet.add(group)) {
				groupList.add(group); // add to list only if it's not a duplicate
			}
		}
	}
	
	public String getIssuer() {
		return issuer;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public List<String> getGroups() {
		return groupList;
	}
	
	/* Use this method to convert the token to a string before sending across the wire.
	   If a key is provided, a timestamp and signed hash will be generated and included in the string.
	   If the key is null, any existing timestamp/hash will be included (so you can go from signed string to token and
	   back to signed string without needing the key). */
	public String stringify(PrivateKey key) {
		final String utf8 = "UTF-8";
		final String space = " ";
	
		try {
			StringBuilder sb = new StringBuilder();
			
			sb.append(URLEncoder.encode(issuer, utf8));
			
			sb.append(space);
			sb.append(URLEncoder.encode(subject, utf8));
			
			for(String group : groupList) {
				sb.append(space);
				sb.append(URLEncoder.encode(group, utf8));
			}
			
			if(key != null) {
				// add timestamp
				sb.append(space);
				timestamp = System.currentTimeMillis();
				sb.append(timestamp);
				
				// add signature
				String stuffToHash = sb.toString(); // hash (and sign) everything that has been stringified so far, including timestamp
				sb.append(space); // space separates timestamp from signed hash
				if(Security.getProvider("BC") == null) {
					Security.addProvider(new BouncyCastleProvider());
				}
				try {
					byte[] hashedStuff = MessageDigest.getInstance("SHA-1", "BC").digest(stuffToHash.getBytes(utf8));
					Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
					cipher.init(Cipher.ENCRYPT_MODE, key);
					signedHash = cipher.doFinal(hashedStuff);
					sb.append(hashToString(signedHash));
				} catch (Exception e) { e.printStackTrace(); return null; }
			}
			else { // no key provided; if it has been previously signed, output that signature
				// add timestamp
				sb.append(space);
				if(timestamp != null) {
					sb.append(timestamp);
				}
				else {
					sb.append("null");
				}
				
				// add signature
				sb.append(space);
				if(signedHash != null) {
					sb.append(hashToString(signedHash)); // ISO-8859-1 is a one-byte encoding and is good for going from bytes to chars and back
				}
				else {
					sb.append("null");
				}
			}
			
			return sb.toString();
		} catch (java.io.UnsupportedEncodingException e) { return null; }
	}
	
	public String toString() {
		return stringify(null);
	}
	
	/* Resurrect a token from its stringified version.
	   If timestampTolerance is positive, then it represents the number of milliseconds after the string's timestamp the string should be rejected.
	   Otherwise (i.e. if timestampTolerance is 0 or negative) the timestamp is ignored.
	   If a key is provided (i.e. not null), the signature on the string will be validated, and if invalid will return null. */
	public static Token fromString(String s, long timestampTolerance, PublicKey key) {
		if(s == null) return null;
		
		final String utf8 = "UTF-8";
		
		try {
			String[] split = s.split(" ");
			if(split.length < 4) return null; // required: issuer, subject, timestamp, signed hash
			
			String iss = URLDecoder.decode(split[0], utf8); // get the issuer
			String sub = URLDecoder.decode(split[1], utf8); // get the subject
			List<String> groups = new ArrayList<String>(); // get the groups (see for loop below)
			for(int i = 2; i < split.length - 2; i++) { // issuer, subject, group, group, ..., group, timestamp, signed hash.
				groups.add(URLDecoder.decode(split[i], utf8));
			}
			
			Long tstamp = Long.valueOf(split[split.length-2]); // -2 because timestamp is the second to last thing, just before the signed hash
			if(timestampTolerance > 0) {
				if(tstamp == null) {
					return null; // no timestamp
				}
				long currentTime = System.currentTimeMillis();
				if(currentTime > tstamp + timestampTolerance) {
					return null; // tolerance window has expired
				}
			}
			
			byte[] shash = hashFromString(split[split.length-1]);
			if(key != null) { // need to verify the hash
				String stuffBeforeHash = s.substring(0, s.lastIndexOf(" ")); // get everything before the space preceding the hash
				if(Security.getProvider("BC") == null) {
					Security.addProvider(new BouncyCastleProvider());
				}
				try {
					byte[] hashedStuff = MessageDigest.getInstance("SHA-1", "BC").digest(stuffBeforeHash.getBytes(utf8)); // this is what the hash should be after un-signing
					
					Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
					cipher.init(Cipher.ENCRYPT_MODE, key);
					byte[] unsignedHash = cipher.doFinal(shash);
					
					// hashedStuff is what we think the hash should be; unsignedHash is the signature in the string, after undoing the signing. These should be equal
					if(!Arrays.equals(hashedStuff, unsignedHash)) {
						return null; // signed hash did not match what we expected based on the hash of the string
					}
				} catch (Exception e) { return null; }
				// if we get here, the signed hash has been verified
			}
			
			Token token = new Token(iss, sub, groups);
			token.timestamp = tstamp;
			token.signedHash = shash;
			return token;
		} catch (java.io.UnsupportedEncodingException e) { return null; }
	}
	
	/* take the signed hash and turn it into a string that has no spaces. Use hashFromString to undo this */
	private static String hashToString(byte[] ba) {
		try {
		String s = new String(ba, "ISO-8859-1"); // one-byte encoding
		s = URLEncoder.encode(s, "UTF-8");
		return s;
		} catch (java.io.UnsupportedEncodingException e) { return null; }
	}
	
	/* Undoes what hashToString does */
	private static byte[] hashFromString(String s) {
		try {
		s = URLDecoder.decode(s, "UTF-8");
		return s.getBytes("ISO-8859-1");
		} catch (java.io.UnsupportedEncodingException e) { return null; }
	}
}