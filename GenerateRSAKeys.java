import java.io.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.*;
import org.bouncycastle.jce.provider.*;
import sun.misc.*;

public class GenerateRSAKeys
{
	private PrivateKey privateKey;
	private PublicKey	publicKey;
	public GenerateRSAKeys()
	{
		KeyPair kp;
		try {
			kp = CreateRSAKeys(4096);
		} catch(Exception e) { kp = null; }
		privateKey = kp.getPrivate();
		publicKey = kp.getPublic();
	}
		

	//returns a keypair containing public and private keys
	public static KeyPair CreateRSAKeys(int numBits) throws Exception
	{
		KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
		keygen.initialize(numBits);						
		KeyPair keypair = keygen.generateKeyPair();
		return keypair;
	}

	//input String text (after converting token to string) outputs ciphertext
	//uses base64 encoder to convert to string
	public static String encrypt(String text, PublicKey key) throws Exception
	{
		byte[] input = text.getBytes("ISO-8859-1");
		byte[] cipherText = null;
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

		cipher.init(Cipher.ENCRYPT_MODE, key);
		cipherText = cipher.doFinal(input);
		String cipherString = new String(cipherText, "ISO-8859-1");
		return cipherString;
	}

	public static String decrypt(String text, PrivateKey key) throws Exception
	{
		byte[] encrypted = text.getBytes("ISO-8859-1");
		byte[] decryptedText = null;
		
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
		decryptedText = cipher.doFinal(encrypted);
		
		return new String(decryptedText, "ISO-8859-1");
	}
	
	public PublicKey getPublicKey()
	{
		return publicKey;
	}
	
	public PrivateKey getPrivateKey()
	{
		return privateKey;
	}

}
